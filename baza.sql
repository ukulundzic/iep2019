USE [master]
GO
/****** Object:  Database [Auctions]    Script Date: 02/02/2019 18:13:23 ******/
CREATE DATABASE [Auctions]
GO
ALTER DATABASE [Auctions] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Auctions].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Auctions] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Auctions] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Auctions] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Auctions] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Auctions] SET ARITHABORT OFF 
GO
ALTER DATABASE [Auctions] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Auctions] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Auctions] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Auctions] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Auctions] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Auctions] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Auctions] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Auctions] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Auctions] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Auctions] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Auctions] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [Auctions] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Auctions] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [Auctions] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Auctions] SET  MULTI_USER 
GO
ALTER DATABASE [Auctions] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Auctions] SET ENCRYPTION ON
GO
ALTER DATABASE [Auctions] SET QUERY_STORE = ON
GO
ALTER DATABASE [Auctions] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO)
GO
USE [Auctions]
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_ADAPTIVE_JOINS = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_MEMORY_GRANT_FEEDBACK = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET BATCH_MODE_ON_ROWSTORE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET DEFERRED_COMPILATION_TV = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ELEVATE_ONLINE = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ELEVATE_RESUMABLE = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET GLOBAL_TEMPORARY_TABLE_AUTO_DROP = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET INTERLEAVED_EXECUTION_TVF = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ISOLATE_SECURITY_POLICY_CARDINALITY = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LIGHTWEIGHT_QUERY_PROFILING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET OPTIMIZE_FOR_AD_HOC_WORKLOADS = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET ROW_MODE_MEMORY_GRANT_FEEDBACK = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET XTP_PROCEDURE_EXECUTION_STATISTICS = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET XTP_QUERY_EXECUTION_STATISTICS = OFF;
GO
USE [Auctions]
GO
/****** Object:  Table [dbo].[Auction]    Script Date: 02/02/2019 18:13:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Auction](
	[Id] [uniqueidentifier] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Duration] [int] NOT NULL,
	[StartPrice] [int] NOT NULL,
	[CurrentPrice] [int] NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[OpeningDate] [datetime] NOT NULL,
	[ClosingDate] [datetime] NOT NULL,
	[StateId] [int] NOT NULL,
	[Image] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Auction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuctionState]    Script Date: 02/02/2019 18:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuctionState](
	[Id] [int] NOT NULL,
	[State] [varchar](20) NOT NULL,
 CONSTRAINT [PK_AuctionState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bid]    Script Date: 02/02/2019 18:13:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bid](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[AuctionId] [uniqueidentifier] NOT NULL,
	[Time] [datetime] NOT NULL,
	[Tokens] [int] NOT NULL,
 CONSTRAINT [PK_Bid] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Constant]    Script Date: 02/02/2019 18:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Constant](
	[Key] [nchar](10) NOT NULL,
	[Value] [nchar](10) NULL,
 CONSTRAINT [PK_Constant] PRIMARY KEY CLUSTERED 
(
	[Key] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 02/02/2019 18:13:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [uniqueidentifier] NOT NULL,
	[UserId] [int] NOT NULL,
	[Tokens] [int] NOT NULL,
	[Price] [int] NOT NULL,
	[StateId] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderState]    Script Date: 02/02/2019 18:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderState](
	[Id] [int] NOT NULL,
	[State] [varchar](20) NOT NULL,
 CONSTRAINT [PK_OrderState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 02/02/2019 18:13:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](20) NOT NULL,
	[LastName] [nvarchar](20) NOT NULL,
	[Password] [nvarchar](1000) NOT NULL,
	[Tokens] [int] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'bf9fcb32-01eb-4260-ace4-3bf3b1bcacd9', N'Aukcija5', 20, 100, 1400, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-08-13T00:00:00.000' AS DateTime), 2, N'~/Content/themes/images/ladies/5.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'7f2a7417-2710-4b40-88ca-460b5eb48203', N'Aukcija2', 20, 100, 1100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-08-10T00:00:00.000' AS DateTime), 2, N'~/Content/themes/images/ladies/2.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'd836b425-60b9-4cbd-8e89-5fc2d1d2cf04', N'Aukcija8', 86400, 100, 100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2019-02-02T17:02:17.377' AS DateTime), CAST(N'2019-02-03T17:02:17.377' AS DateTime), 2, N'~/Content/themes/images/ladies/8.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'c5bb999b-3991-4f83-811c-7f4da962324e', N'Aukcija9', 86400, 100, 100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2019-02-02T17:02:22.223' AS DateTime), CAST(N'2019-02-03T17:02:22.223' AS DateTime), 2, N'~/Content/themes/images/ladies/9.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'686a3e28-5eb5-46c5-b3ed-94d1984a2961', N'Aukcija1', 20, 100, 1000, CAST(N'2018-07-09T16:41:56.380' AS DateTime), CAST(N'2019-02-01T21:21:25.717' AS DateTime), CAST(N'2019-02-01T21:21:45.717' AS DateTime), 2, N'~/Content/themes/images/ladies/1.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'47f5a3e7-3666-416e-871e-9f43fa692b6b', N'Aukcija4', 20, 100, 100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2019-02-02T17:02:25.683' AS DateTime), CAST(N'2019-02-02T17:02:45.683' AS DateTime), 2, N'~/Content/themes/images/ladies/4.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'ff54b92d-d15d-4076-8e05-b016c8566851', N'Aukcija10', 1900, 100, 100, CAST(N'2019-01-27T22:29:02.867' AS DateTime), CAST(N'2019-02-02T17:02:29.607' AS DateTime), CAST(N'2019-02-02T17:34:09.607' AS DateTime), 2, N'~/Content/themes/images/men/10.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'408d5965-1818-4612-8c75-b436f5b8b7e5', N'Aukcija6', 86400, 100, 100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2019-02-02T17:02:32.720' AS DateTime), CAST(N'2019-02-03T17:02:32.720' AS DateTime), 2, N'~/Content/themes/images/ladies/6.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'cdb1b633-c1f0-4666-bc22-b7052864461d', N'Aukcija7', 86400, 100, 100, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2019-02-02T17:02:39.340' AS DateTime), CAST(N'2019-02-03T17:02:39.340' AS DateTime), 2, N'~/Content/themes/images/ladies/7.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'6d23ba16-d8c6-4a02-9f6a-e76d43ffb1fb', N'Aukcija3', 20, 100, 1200, CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-07-09T16:41:56.383' AS DateTime), CAST(N'2018-08-11T00:00:00.000' AS DateTime), 3, N'~/Content/themes/images/ladies/3.jpg')
INSERT [dbo].[Auction] ([Id], [Name], [Duration], [StartPrice], [CurrentPrice], [CreationDate], [OpeningDate], [ClosingDate], [StateId], [Image]) VALUES (N'0344b702-84da-4dda-b469-f02dec797359', N'Aukcija11', 200, 100, 100, CAST(N'2019-01-31T14:11:43.300' AS DateTime), CAST(N'2019-01-31T14:11:43.610' AS DateTime), CAST(N'2019-01-31T14:11:43.917' AS DateTime), 1, N'~/Content/themes/images/men/11.jpg')
INSERT [dbo].[AuctionState] ([Id], [State]) VALUES (1, N'READY')
INSERT [dbo].[AuctionState] ([Id], [State]) VALUES (2, N'OPENED')
INSERT [dbo].[AuctionState] ([Id], [State]) VALUES (3, N'COMPLETED')
SET IDENTITY_INSERT [dbo].[Bid] ON 

INSERT [dbo].[Bid] ([Id], [UserId], [AuctionId], [Time], [Tokens]) VALUES (1, 3, N'c5bb999b-3991-4f83-811c-7f4da962324e', CAST(N'2019-01-31T15:10:07.237' AS DateTime), 5)
INSERT [dbo].[Bid] ([Id], [UserId], [AuctionId], [Time], [Tokens]) VALUES (2, 3, N'408d5965-1818-4612-8c75-b436f5b8b7e5', CAST(N'2019-01-31T15:12:27.813' AS DateTime), 7)
INSERT [dbo].[Bid] ([Id], [UserId], [AuctionId], [Time], [Tokens]) VALUES (3, 3, N'6d23ba16-d8c6-4a02-9f6a-e76d43ffb1fb', CAST(N'2019-01-31T15:12:27.850' AS DateTime), 6)
INSERT [dbo].[Bid] ([Id], [UserId], [AuctionId], [Time], [Tokens]) VALUES (4, 4, N'408d5965-1818-4612-8c75-b436f5b8b7e5', CAST(N'2019-01-31T15:12:59.063' AS DateTime), 8)
SET IDENTITY_INSERT [dbo].[Bid] OFF
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'C         ', N'RSD       ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'D         ', N'86400     ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'G         ', N'50        ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'N         ', N'10        ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'P         ', N'100       ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'S         ', N'30        ')
INSERT [dbo].[Constant] ([Key], [Value]) VALUES (N'T         ', N'100       ')
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'70923c83-7c0a-4b47-ac74-049e05266a77', 3, 3, 150, 1)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'ee516ac7-16d0-434c-9000-1751a9e13b50', 4, 3, 150, 3)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'd20f10e4-ba7c-4c1a-abd0-38a0c288a7b6', 3, 1, 50, 2)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'aa2aff70-e858-4e50-b38b-59f19ddef5fd', 4, 1, 50, 1)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'5cae63e5-9b49-4aa8-875b-74ea06276721', 7, 1000, 100000, 1)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'fe4ea59a-1df7-4bd6-83e7-8e3ba37d6b85', 4, 2, 100, 2)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'5b4a810c-10ef-4549-ade4-b1f994b77a76', 3, 2, 100, 3)
INSERT [dbo].[Order] ([Id], [UserId], [Tokens], [Price], [StateId]) VALUES (N'5902bc3f-7ace-44ee-be49-d75c3ac5f55c', 7, 1000, 100000, 3)
INSERT [dbo].[OrderState] ([Id], [State]) VALUES (1, N'SUBMITTED')
INSERT [dbo].[OrderState] ([Id], [State]) VALUES (2, N'CANCELED')
INSERT [dbo].[OrderState] ([Id], [State]) VALUES (3, N'COMPLETED')
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([Id], [Email], [FirstName], [LastName], [Password], [Tokens]) VALUES (3, N'user@userovic.user', N'User', N'Userovic', N'hbdDRjxGscwwrQJyRZs10F073DBTq/KzqlHa7o2wQG/4wAAJvzP3NEsbxwPDSBOlBsI8B7/UhbDF3rF44L3PpbKQUB+d5g==', 1000)
INSERT [dbo].[User] ([Id], [Email], [FirstName], [LastName], [Password], [Tokens]) VALUES (4, N'uroskulundzic@gmail.com', N'Uros', N'Kulundzic', N'SZNLoWCXWe3WTXUPHkdNFqnAfEx02iI6fvrgQJuq99Z4oyXWPIvP9oOnpdXxCOdIVsQr/cPI+2D6AG8mYus0OShemlQ=', 0)
INSERT [dbo].[User] ([Id], [Email], [FirstName], [LastName], [Password], [Tokens]) VALUES (5, N'petar@etf.rs', N'Petar', N'Petrovic', N'o2J9Dlc28X9kX1FDKfkEwDV9CAi4Nani6AnwtOKskyNX8rUBD21YoP3BH/o684nJ373HXrj9cPQ3lYn0GHJXfXTKO8g=', 0)
INSERT [dbo].[User] ([Id], [Email], [FirstName], [LastName], [Password], [Tokens]) VALUES (6, N'ADMIN', N'ADMIN', N'ADMINOVIC', N'vf8naX9t5HJt3HCgzEdaed8XFgYLv05qSm7xeFgGuqaoHIS10bLIUVjSF/cqUnxXCVBpMAOOTHu+cmRQy774Ft6+xKofLw==', 0)
INSERT [dbo].[User] ([Id], [Email], [FirstName], [LastName], [Password], [Tokens]) VALUES (7, N'nemanja.kojic@etf.rs', N'Nemanja', N'Kojic', N'yO+X7HcFi9zo9+rgpzFGZtqYtsd/rmI5yGB8YhYLclQBiLEc5drqZXCOAmlwO77rBgGJFVWTdsM0SfGwraHRSVVDNpAF', 1000)
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Auction]  WITH CHECK ADD  CONSTRAINT [FK_Auction_AuctionState] FOREIGN KEY([StateId])
REFERENCES [dbo].[AuctionState] ([Id])
GO
ALTER TABLE [dbo].[Auction] CHECK CONSTRAINT [FK_Auction_AuctionState]
GO
ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_Auction] FOREIGN KEY([AuctionId])
REFERENCES [dbo].[Auction] ([Id])
GO
ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_Auction]
GO
ALTER TABLE [dbo].[Bid]  WITH CHECK ADD  CONSTRAINT [FK_Bid_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Bid] CHECK CONSTRAINT [FK_Bid_User]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderState] FOREIGN KEY([StateId])
REFERENCES [dbo].[OrderState] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderState]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_User]
GO
USE [master]
GO
ALTER DATABASE [Auctions] SET  READ_WRITE 
GO
