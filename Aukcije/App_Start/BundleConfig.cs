﻿using System.Web;
using System.Web.Optimization;

namespace Aukcije
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Content/themes/js/jquery-1.7.2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/AllScripts").Include(
                      "~/Content/themes/js/common.js",
                      "~/Content/themes/js/jquery.fancybox.js",
                      "~/Content/themes/js/jquery.flexslider-min.js",
                      "~/Content/themes/js/jquery.scrolltop.js",
                      "~/Content/themes/js/superfish.js"
                ));
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/css/bootstrap.min.css",
                      "~/Content/bootstrap/css/bootstrap-responsive.min.css",
                      "~/Content/themes/css/bootstrappage.css",
                      "~/Content/themes/css/flexslider.css",
                      "~/Content/themes/css/jquery.fancybox.css",
                      "~/Content/themes/css/main.css",
                      "~/Content/themes/js/css/style.css",
                      "~/Content/site.css"));
        }
    }
}
