﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aukcije.DataModel;
using BusinessLogic;

namespace Aukcije
{
    public class UserEnvironment : IUserEnvironment
    {
        public static UserEnvironment Current
        {
            get
            {
                UserEnvironment result = null;
                if(HttpContext.Current!=null)
                {
                    if(HttpContext.Current.Session !=null)
                    {
                        result = HttpContext.Current.Session["CurrentUserEnvironment"] as UserEnvironment;
                    }
                }
                if (result != null)
                    return result;
                result = new UserEnvironment();
                Current = result;
                return result;
            }
            set
            {
                if(HttpContext.Current!=null)
                {
                    if (HttpContext.Current.Session != null)
                        HttpContext.Current.Session["CurrentUserEnvironment"] = value;
                }
            }
        }
        public bool IsUserLoggedIn
        {
            get
            {
                return HttpContext.Current.User.Identity.IsAuthenticated;
            }
        }
        public bool IsAdmin
        {
            get
            {
                return new UserBusinessLogic().CheckIfAdmin(_user);
            }
        }
        protected User _user;
        public User User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }
        public bool IsActionAvailable(string id)
        {
            throw new NotImplementedException();
        }

        public bool IsActionAvailable(string id, string action)
        {
            throw new NotImplementedException();
        }
    }
}