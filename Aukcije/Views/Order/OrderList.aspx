﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderList.aspx.cs" Inherits="Aukcije.Views.Order.OrderList" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id" DataSourceID="SqlDataSource1" GridLines="Vertical" Height="317px" Width="1204px" HorizontalAlign="Center" PageSize="5">
                <AlternatingRowStyle BackColor="Gainsboro" />
                <Columns>
                    <asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />
                    <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="Tokens" HeaderText="Tokens" SortExpression="Tokens" />
                    <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DataModelContext %>" OnSelecting="SqlDataSource1_Selecting"> <%--SelectCommand="SELECT OrderState.State, [Order].Id, [Order].Tokens, [Order].Price FROM [Order] INNER JOIN OrderState ON [Order].StateId = OrderState.Id INNER JOIN [User] ON [Order].UserId = [User].Id WHERE ([User].Id = @Param1)">--%>
              <%-- 
                <SelectParameters>
                    <asp:Parameter Name="userId" />
                </SelectParameters>--%>
            </asp:SqlDataSource>
            <%--SqlDataSource1.SelectParameters.Add("userid", DbType.Guid, userId.ToString());--%>
        </div>
    </form>
</body>
</html>
