﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Aukcije.Views.Order
{
    public partial class OrderList : System.Web.Mvc.ViewPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlDataSource1.SelectParameters.Add("Param1", UserEnvironment.Current.User.Id.ToString());
            SqlDataSource1.SelectCommand="SELECT OrderState.State, [Order].Id, [Order].Tokens, [Order].Price FROM [Order] INNER JOIN OrderState ON [Order].StateId = OrderState.Id INNER JOIN [User] ON [Order].UserId = [User].Id WHERE ([User].Id = @Param1)";
        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
           
        }
    }
}