namespace Aukcije.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bid")]
    public partial class Bid
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public Guid AuctionId { get; set; }

        public DateTime Time { get; set; }

        public int Tokens { get; set; }

        public virtual Auction Auction { get; set; }

        public virtual User User { get; set; }
    }
}
