﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aukcije.DataModel
{
    public interface IUserEnvironment
    {
        bool IsUserLoggedIn { get; }
        bool IsAdmin { get; }
        User User { get; set; }
        bool IsActionAvailable(string id);
        bool IsActionAvailable(string id, string action);
    }
}
