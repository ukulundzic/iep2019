namespace Aukcije.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Order")]
    public partial class Order
    {
        public Guid Id { get; set; }

        public int UserId { get; set; }

        public int Tokens { get; set; }

        public int Price { get; set; }

        public int StateId { get; set; }

        public virtual OrderState OrderState { get; set; }

        public virtual User User { get; set; }
    }
}
