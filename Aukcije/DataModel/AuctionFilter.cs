﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aukcije.DataModel
{
    public partial class AuctionFilter
    {
        public string Name { get; set; }
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public AuctionStateEnum State { get; set; }
    }
    public enum AuctionStateEnum
    {
        ALL,
        READY,
        OPENED,
        COMPLETED
    }
}