namespace Aukcije.DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DataModelContext : DbContext
    {
        public DataModelContext()
            : base("name=DataModelContext")
        {
        }

        public virtual DbSet<Auction> Auctions { get; set; }
        public virtual DbSet<AuctionState> AuctionStates { get; set; }
        public virtual DbSet<Bid> Bids { get; set; }
        public virtual DbSet<Constant> Constants { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderState> OrderStates { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Auction>()
                .HasMany(e => e.Bids)
                .WithRequired(e => e.Auction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AuctionState>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<AuctionState>()
                .HasMany(e => e.Auctions)
                .WithRequired(e => e.AuctionState)
                .HasForeignKey(e => e.StateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Constant>()
                .Property(e => e.Key)
                .IsFixedLength();

            modelBuilder.Entity<Constant>()
                .Property(e => e.Value)
                .IsFixedLength();

            modelBuilder.Entity<OrderState>()
                .Property(e => e.State)
                .IsUnicode(false);

            modelBuilder.Entity<OrderState>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.OrderState)
                .HasForeignKey(e => e.StateId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Bids)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
