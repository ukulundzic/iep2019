namespace Aukcije.DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Constant")]
    public partial class Constant
    {
        [Key]
        [StringLength(10)]
        public string Key { get; set; }

        [StringLength(10)]
        public string Value { get; set; }
    }
}
