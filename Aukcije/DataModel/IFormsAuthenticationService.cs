﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;
namespace Aukcije.DataModel
{
    public interface IFormsAuthenticationService
    {
        void SignIn(string userName, bool createPersistentCookie);
        void SignOut();
    }
    public class FormsAuthenticationService : IFormsAuthenticationService
    {
        public void SignIn(string userId, bool createPersistentCookie)
        {
            if (String.IsNullOrEmpty(userId))
                throw new ArgumentException("Value cannot be null or empty.", "userId");
            FormsAuthentication.SetAuthCookie(userId, createPersistentCookie);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
        }
    }
}
