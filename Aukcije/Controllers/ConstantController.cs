﻿using BusinessLogic;
using Aukcije.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aukcije.Controllers
{
    public class ConstantController : Controller
    {
        private ConstantBusinessLogic constantBusinessLogic;
        public ConstantController()
        {
            constantBusinessLogic = new ConstantBusinessLogic();
        }
        public ActionResult Index()
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            else if (!UserEnvironment.Current.IsAdmin)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be admin" });
            }
            return View(constantBusinessLogic.GetAllConstants());
        }
        [HttpPost]
        public ActionResult Change(List<Constant> constants)
        {
            try
            {
                constantBusinessLogic.UpdateParameters(constants);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index", "Constant", new { error = "Error while editing parameters!" });
            }
            return RedirectToAction("Index", "Home", new { success = "You have succcessfuly changed the application parameters!" });
        }
    }
}