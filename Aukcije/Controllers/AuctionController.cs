﻿using Aukcije.DataModel;
using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Aukcije.Controllers
{
    public class AuctionController : Controller
    {
        private AuctionBusinessLogic _auctionBusinessLogic;
        private BidBusinessLogic bidBusinessLogic;
        public AuctionController()
        {
            _auctionBusinessLogic = new AuctionBusinessLogic();
            bidBusinessLogic = new BidBusinessLogic();
        }
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.Browse = false;
            return View();
        }
        [HttpPost]
        public ActionResult Index(List<Auction> lista)
        {
            ViewBag.Browse = true;
            ViewBag.Model = lista;
            return View();
        }
        [HttpPost]
        public ActionResult Browse(AuctionFilter model)
        {
            if(model.MinPrice>model.MaxPrice)
            {
                return RedirectToAction("Index", new { error = "Invalid price range parameters!" });
            }
            if (model.MaxPrice == 0) model.MaxPrice = int.MaxValue;
            List<Auction> lista = _auctionBusinessLogic.GetItems(model);
            ViewBag.Browse = true;
            ViewBag.Model = lista;
            return View(lista);
        }
        [HttpGet]
        public ActionResult GetBidder(string id)
        {
            Bid bidder = _auctionBusinessLogic.GetBidder(id);
            return Json(bidder!=null?bidder.User.FirstName+" "+bidder.User.LastName:null, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Bid(string id, int value)
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return Json(new { success = false, responseText = "You must be logged in!" }, JsonRequestBehavior.AllowGet);
            }
            Auction auction = _auctionBusinessLogic.GetById(new Guid(id));
            try
            {
                bidBusinessLogic.CreateBid(UserEnvironment.Current.User, auction, value);
            }
            catch(Exception e)
            {
                return Json(new { success = false, responseText = "Error while bidding! Check if you have enough tokens on your account!" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, responseText = UserEnvironment.Current.User.FirstName+ " " +UserEnvironment.Current.User.LastName }, JsonRequestBehavior.AllowGet);

            //Bid bidder = _auctionBusinessLogic.GetBidder(id);
            //return Json(bidder != null ? bidder.User.FirstName + " " + bidder.User.LastName : null, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Close(string id)
        {
            try
            {
                _auctionBusinessLogic.CloseAuction(new Guid(id));
            }
            catch (Exception e)
            {
                return Json(new { success = false, responseText = "Error" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { success = true, responseText = "CLOSED" }, JsonRequestBehavior.AllowGet);

        }
        public ActionResult Detail(Guid id)
        {
            Auction model = _auctionBusinessLogic.GetById(id);
            return View(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            return View();
        }
        [HttpPost]
        public ActionResult Create(Auction item)
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            if (_auctionBusinessLogic.ValidateNewAuction(item))
            {
                return RedirectToAction("Create", "Auction", new { error = "Error while creating auction! Please fill all the fields in order!" });
            }
            try
            {
                _auctionBusinessLogic.CreateNewAuction(item);
            }
            catch(Exception e)
            {
                return RedirectToAction("Create", "Auction", new { error = "Error while creating auction! Please fill all the fields in order or try again later!" });
            }
            return RedirectToAction("Index", "Auction", new { success = "Successfully created a new auction!" });

        }
        [HttpGet]
        public ActionResult ListWon()
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            List<Auction> lista = _auctionBusinessLogic.GetListOfWonAuctions(UserEnvironment.Current.User.Id);
            ViewBag.Model = lista;
            return View("Browse",lista);
        }
        [HttpGet]
        public ActionResult OpenAuctions()
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            else if (!UserEnvironment.Current.IsAdmin)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be admin" });
            }
            List<Auction> list = _auctionBusinessLogic.GetListOfOpenAuctions();
            ViewBag.Model = list;
            return View(list);
        }

        [HttpGet]
        public ActionResult OpenAuction(string auctionId)
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            else if (!UserEnvironment.Current.IsAdmin)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be admin" });
            }
            try
            {
                _auctionBusinessLogic.OpenAuction(new Guid(auctionId));
            }
            catch (Exception e)
            {
                return RedirectToAction("OpenAuctions", "Auction", new { error = "Error while opening auction!" });
            }
            return RedirectToAction("Index", "Home", new { success = "Successfully opened an auction!" });
        }
    }
}