﻿using BusinessLogic;
using Aukcije.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Aukcije.Controllers
{
    public class UserController : Controller
    {
        public IFormsAuthenticationService FormsService { get; set; }
        private UserBusinessLogic _userBusinessLogic;
        protected override void Initialize(RequestContext requestContext)
        {
            if (FormsService == null) { FormsService = new FormsAuthenticationService(); }
            base.Initialize(requestContext);
        }
        public UserController()
        {
            _userBusinessLogic = new UserBusinessLogic();
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Register()
        {
            if (UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Login()
        {
            if(UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(User user)
        {
            if (UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            bool userDoesntExist = false;
            User loggedUser = _userBusinessLogic.GetUserByEmail(user.Email);
            if (loggedUser == null) userDoesntExist = true;
            if (!userDoesntExist)
            {
                if (Helper.VerifyHash(user.Password,"SHA512",loggedUser.Password))
                {
                    UserEnvironment.Current.User = loggedUser;
                    FormsService.SignIn(user.Id.ToString(), true);
                }
                else
                {
                    return RedirectToAction("Login", new { error = "Wrong password!" });
                }
            }
            else return RedirectToAction("Login",new { error="User does not exist!"});
            
            return RedirectToAction("Index","Home",new { success="Successfully logged in!"});
        }
        public ActionResult LogOff()
        {
            UserEnvironment.Current = null;
            FormsService.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public ActionResult Register(User user)
        {
            if (UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "Home");
            }
            user.Tokens = 0;
            if(_userBusinessLogic.Validate(user))
            {
                return RedirectToAction("Register",new {error = "Error! All fields are required! The email address must not already be in use!" });
            }
            user.Password = Helper.ComputeHash(user.Password,"SHA512",null);
            _userBusinessLogic.Insert(user);
            return RedirectToAction("Index", new { success = "Successfull registration!" });
        }
        [HttpGet]
        public ActionResult Change()
        {
            if(!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User",new {message="You have to be logged in" });
            }
            return View(UserEnvironment.Current.User);
        }
        [HttpPost]
        public ActionResult Change(User user)
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            if(_userBusinessLogic.Validate(user))
            {
                return RedirectToAction("Change", new { error = "Invalid input!" });
            }
            user.Password = Helper.ComputeHash(user.Password, "SHA512", null);
            _userBusinessLogic.Update(user,UserEnvironment.Current.User.Id);
            UserEnvironment.Current.User = user;
            return RedirectToAction("Index","Home",new { success="Changes were successfully made!"});
        }
    }
    
}