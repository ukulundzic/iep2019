﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aukcije.DataModel;
using BusinessLogic;

namespace Aukcije.Controllers
{
    public class OrderController : Controller
    {
        private OrderBusinessLogic orderBusinessLogic;
        public OrderController()
        {
            orderBusinessLogic = new OrderBusinessLogic();
        }
        [HttpGet]
        public ActionResult Index()
        {
            if (!UserEnvironment.Current.IsUserLoggedIn)
            {
                return RedirectToAction("Index", "User", new { message = "You have to be logged in" });
            }
            if(orderBusinessLogic.CheckIfOrdersExist(UserEnvironment.Current.User.Id))
            {
                return View("OrderList");
            }
            return View();
        }
    }
}