﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aukcije.DataModel;
using System.Data.Entity;
using System.IO;

namespace BusinessLogic
{
    public class AuctionStateBusinessLogic
    {
        public AuctionState GetStateForEnum(AuctionStateEnum auctionStateEnum)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.AuctionStates.Where(x => x.State.Equals(auctionStateEnum.ToString())).Single();
            }
        }
    }
}
