﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aukcije.DataModel;
using System.Data.Entity;

namespace BusinessLogic
{
    public class OrderBusinessLogic
    {
        public List<Order> getAllOrders(User user)
        {
            if (user==null) return null;
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Orders.Where(x => x.UserId == user.Id).Include(x=>x.OrderState).ToList();
            }
        }
        public bool CheckIfOrdersExist(int id)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Orders.Where(x => x.UserId == id).Any();
            }
        }
    }
}
