﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.IO;
using Aukcije.DataModel;

namespace BusinessLogic
{
    public class AuctionBusinessLogic
    {
        private AuctionStateBusinessLogic auctionStateBusinessLogic;
        private ConstantBusinessLogic constantBusinessLogic;
        public AuctionBusinessLogic()
        {
            auctionStateBusinessLogic = new AuctionStateBusinessLogic();
            constantBusinessLogic = new ConstantBusinessLogic();
        }
        public List<Auction> GetItems(AuctionFilter filter)
        {
            using (var dbctx = new DataModelContext())
            {
                string[] words=null;
                string query = "SELECT * FROM dbo.Auction ";
                if (filter.Name != null)
                {
                    words = filter.Name.Split(' ');
                    for (int i = 0; i < words.Length; i++)
                    {
                        if (i == 0) query += "WHERE ";
                        else query += "AND ";
                        string word = words[i];
                        query += "dbo.Auction.Name LIKE '%" + word + "%' ";
                    }
                }
                if(words==null)
                {
                    query += "WHERE ";
                    query += "dbo.Auction.CurrentPrice >= " + filter.MinPrice;
                }
                else query += "AND dbo.Auction.CurrentPrice >= " + filter.MinPrice;
                query += " AND dbo.Auction.CurrentPrice <= " + filter.MaxPrice;
                if(filter.State!=AuctionStateEnum.ALL)
                {
                    int stanje=0;
                    if (filter.State == AuctionStateEnum.READY) stanje = 1;
                    else if (filter.State == AuctionStateEnum.OPENED) stanje = 2;
                    else stanje = 3;
                    query += " AND dbo.Auction.StateId = " + stanje;
                }
                return dbctx.Auctions.SqlQuery(query).OrderByDescending(x=>x.ClosingDate).Take(Int32.Parse(constantBusinessLogic.GetValueForKey("N"))).ToList();
            }
        }
        public Bid GetBidder(string id)
        {
            using (var dbctx = new DataModelContext())
            {
                Guid guid = new Guid(id);
                return dbctx.Bids.Where(x => x.AuctionId == guid).OrderByDescending(x => x.Tokens).Include(x=>x.User).FirstOrDefault();
            }
        }
        public Auction GetById(Guid id)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Auctions.Where(x => x.Id == id).Include(x => x.Bids).Include(x=>x.AuctionState).Single();
            }
        }
        public bool ValidateNewAuction(Auction item)
        {
            bool error = false;
            if (string.IsNullOrEmpty(item.Name)) error = true;
            if (string.IsNullOrEmpty(item.Image)) error = true;
            else if (!File.Exists(item.Image)) error = true;
            return error;
        }
        public void CreateNewAuction(Auction auction)
        {
            using (var dbctx = new DataModelContext())
            {
                auction.StateId = 1;
                auction.CreationDate = DateTime.Now;
                auction.OpeningDate = DateTime.Now;
                auction.ClosingDate = DateTime.Now;
                auction.Id = Guid.NewGuid();
                auction.CurrentPrice = auction.StartPrice;
                auction.Image = auction.Image.Replace('\\', '/');
                auction.Image = auction.Image.Substring(auction.Image.IndexOf("/Content"));
                auction.Image = "~" + auction.Image;
                if (auction.Duration == 0) auction.Duration = Int32.Parse(constantBusinessLogic.GetValueForKey("D"));
                dbctx.Auctions.Add(auction);
                dbctx.SaveChanges();
            }
        }
        public List<Auction> GetListOfWonAuctions(int userId)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Auctions.Include(x=>x.Bids).Where(x => x.AuctionState.Id == 3).Where(x => x.Bids.OrderByDescending(y=>y.Tokens).FirstOrDefault().UserId == userId).OrderByDescending(x => x.ClosingDate).ToList();
            }
        }
        public List<Auction> GetListOfOpenAuctions()
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Auctions.Where(x=>x.StateId==1).Any()?dbctx.Auctions.Where(x => x.StateId == 1).ToList():null;
            }
        }
        public void OpenAuction(Guid id)
        {
            using (var dbctx = new DataModelContext())
            {
                Auction auction = dbctx.Auctions.Where(x => x.Id.Equals(id)).Single();
                auction.StateId = 2;
                auction.OpeningDate = DateTime.Now;
                auction.CurrentPrice = auction.StartPrice;
                auction.ClosingDate = auction.OpeningDate.AddSeconds(auction.Duration);
                dbctx.Entry(auction).State = System.Data.Entity.EntityState.Modified;
                dbctx.SaveChanges();
            }
        }
        public void CloseAuction(Guid id)
        {
            using (var dbctx = new DataModelContext())
            {
                Auction auction = dbctx.Auctions.Where(x => x.Id.Equals(id)).Single();
                auction.StateId = 3;
                dbctx.Entry(auction).State = System.Data.Entity.EntityState.Modified;
                dbctx.SaveChanges();
            }
        }
    }
}
