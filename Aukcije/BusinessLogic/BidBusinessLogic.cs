﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Aukcije.DataModel;
using System.Data.Entity;

namespace BusinessLogic
{
    public class BidBusinessLogic
    {
        private UserBusinessLogic userBusinessLogic;
        public BidBusinessLogic()
        {
            userBusinessLogic = new UserBusinessLogic();
        }
        public void CreateBid(User user, Auction auction, int value)
        {
            using (var dbctx = new DataModelContext())
            {
                if (userBusinessLogic.HasEnoughTokens(user.Id,value))
                {
                    Bid bid = new Bid();
                    auction.CurrentPrice = value;
                    bid.AuctionId = auction.Id;
                    bid.UserId = user.Id;
                    bid.Time = DateTime.Now;
                    bid.Tokens = value;
                    dbctx.Entry(bid).State = EntityState.Added;
                    dbctx.Entry(auction).State = EntityState.Modified;
                    dbctx.SaveChanges();
                }
                else throw new Exception();
            }
        }
    }
}