﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aukcije.DataModel;
using System.Data.Entity;

namespace BusinessLogic
{
    public class ConstantBusinessLogic
    {
        public string GetValueForKey(string key)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Constants.Where(x => x.Key.Equals(key)).Single().Value;
            }
        }
        private void SetNewValueForKey(string key, string value)
        {
            using (var dbctx = new DataModelContext())
            {
                Constant constant = dbctx.Constants.Where(x => x.Key.Equals(key)).Single();
                constant.Value = value;
                dbctx.Entry(constant).State = EntityState.Modified;
                dbctx.SaveChanges();
            }
        }
        public List<Constant> GetAllConstants()
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Constants.ToList();
            }
        }
        public void UpdateParameters(List<Constant> constants)
        {
            foreach (var constant in constants) SetNewValueForKey(constant.Key,constant.Value);
        }
    }
}
