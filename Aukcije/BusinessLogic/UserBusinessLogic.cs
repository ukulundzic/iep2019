﻿using Aukcije.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class UserBusinessLogic
    {
        public bool ValidateEmail(string email)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Users.Where(x => x.Email.ToUpper().Equals(email.ToUpper())).Any();
            }
        }
        public bool CheckIfAdmin(User user)
        {
            return user.Email.Equals("ADMIN");
        }
        public bool Validate(User user)
        {
            bool error = false;
            if (string.IsNullOrEmpty(user.FirstName)) error = true;
            if (string.IsNullOrEmpty(user.LastName)) error = true;
            if (string.IsNullOrEmpty(user.Email)) error = true;
            else if (ValidateEmail(user.Email)) error = true;
            if (string.IsNullOrEmpty(user.Password)) error = true;
            return error;
        }
        public User GetUserByEmail(String email)
        {
            using (var dbctx = new DataModelContext())
            {
                if(dbctx.Users.Where(x => x.Email==email).Any())
                    return dbctx.Users.Where(x => x.Email == email).Single();
                return null;
            }
        }
        public User GetUser(int id)
        {
            using (var dbctx = new DataModelContext())
            {
                if (dbctx.Users.Where(x => x.Id == id).Any())
                    return dbctx.Users.Where(x => x.Id == id).Single();
                return null;
            }
        }
        public void Insert(User user)
        {
            using (var dbctx = new DataModelContext())
            {
                dbctx.Users.Add(user);
                dbctx.SaveChanges();
            }
        }
        public void Update(User user,int id)
        {
            using (var dbctx = new DataModelContext())
            {
                User temp = GetUser(id);
                temp.FirstName = user.FirstName;
                temp.LastName = user.LastName;
                temp.Email = user.Email;
                temp.Password = user.Password;
                dbctx.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                dbctx.SaveChanges();
            }
        }
        public bool HasEnoughTokens(int id, int tokens)
        {
            using (var dbctx = new DataModelContext())
            {
                return dbctx.Users.Where(x => x.Id == id).Where(x => x.Tokens >= tokens).Any();
            }
        }
    }
}
